import requests
import sys
import subprocess

# API_KEY and USRNR from https://sailaway.world/cgi-bin/sailaway.world/myaccount.pl
API_KEY = '857C9C7CC087F086EEA550ABCA5EB8BC'
USRNR = '2462'
API_BASE_URL = 'http://srv.sailaway.world/cgi-bin/sailaway'
MENU_OPTIONS = {}
STELLARIUM = "C:\\Program Files\\Stellarium\\stellarium.exe"

# Load user´s boat data from Sailaway API and return as JSON object
def loadData():
    params = dict(
    key=API_KEY,
    usrnr=USRNR,
    )
    url = f'{API_BASE_URL}/APIBoatInfo.pl'
    resp = requests.get(url=url, params=params)
    result = resp.json()
    return result

# Return geographical position of a named boat
def getBoatPosition(data, boatname):
    for boat in data:
        if boat['boatname'] == boatname:
            lat = boat['latitude']
            lon = boat['longitude']
    return(['--longitude=' + str(lon), '--latitude='+ str(lat)])

# Convert boat into Dict for menu structure
def allBoats2MenuDict(data):
    count = 1
    for boat in data:
        if boat['spd'] > 0:
            MENU_OPTIONS[count] = boat['boatname']
            count += 1
    MENU_OPTIONS[count] = "Exit"


# Display menu
def print_menu():
    for key in MENU_OPTIONS.keys():
        print (key, '--', MENU_OPTIONS[key] )

# Program starts here
if __name__ == "__main__":
    data = loadData()
    allBoats2MenuDict(data)
    while(True):
        print_menu()
        option = ''
        try:
            option = int(input('Enter your choice: '))
        except:
            print('Wrong input. Please enter a number between 1 and ' + str(len(MENU_OPTIONS)))
            continue
        if option > len(MENU_OPTIONS) or option < 1:
            print('Wrong input. Please enter a number between 1 and ' + str(len(MENU_OPTIONS)))
            continue
        elif option == len(MENU_OPTIONS):
            print('Good bye')
            sys.exit()
        else:
            pos = getBoatPosition(data, MENU_OPTIONS[option])
            print('Launching ' + STELLARIUM + ' with position ' + str(pos))
            subprocess.Popen([STELLARIUM] + pos)
            sys.exit()
