import os
import pathlib
import platform
import time
from datetime import datetime, timedelta
import requests
import json
import logging
import graypy
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from dotenv import load_dotenv
from statusbadge import statusBadge
from dtl import haversine
import getMissionsFromSARL
import termtables as tt

load_dotenv()

DATAPATH = os.getenv('DATAPATH')
API_BASE_URL = os.getenv('SAILAWAY_API_BASE_URL')
API_KEY = os.getenv('SAILAWAY_API_KEY')
API_REQUEST_INTERVAL = int(os.getenv('API_REQUEST_INTERVAL'))

MISSION = os.getenv('MISSION')
# LOG_SERVER_ADDRESS = os.getenv('GRAYLOG_SERVER')
# LOG_SERVER_PORT = int(os.getenv('GRAYLOG_PORT'))

# my_logger = logger = logging.getLogger(__name__)
# my_logger.setLevel(logging.DEBUG)
# handler = graypy.GELFUDPHandler(LOG_SERVER_ADDRESS, LOG_SERVER_PORT)
# my_logger.addHandler(handler)


def getAPIData(racenr: str, type='lb'):
    print(f'Sailaway API request, type: {type}')
    if type == 'lb':
        filename = f'{DATAPATH}/data_{racenr}.json'
        endpoint = f'{API_BASE_URL}/GetLeaderboard.pl'
    elif type == 'marks':
        filename = f'{DATAPATH}/marks_{racenr}.json'
        endpoint = f'{API_BASE_URL}/GetMissionCourse.pl'
    elif type == 'racelist':
        filename = f'{DATAPATH}/sa-races.json'
        endpoint = f'{API_BASE_URL}/GetMissions.pl'

    p = pathlib.Path(filename)
    if p.is_file():
        lastRequest = datetime.fromtimestamp(creation_date(filename))
        timeDelta = datetime.now() - lastRequest
    else:
        timeDelta = timedelta(minutes=60)

    if timeDelta.total_seconds() < API_REQUEST_INTERVAL:
        print(f'Getting data from cached file, age: {int(timeDelta.total_seconds())} seconds')
        with open(filename) as json_file:
            data = json.load(json_file)
    else:
        # Delete old data
        if os.path.exists(filename):
            os.remove(filename)
        print(f'Requesting data from Sailaway API, age: {int(timeDelta.total_seconds())} seconds')
        if type != 'racelist':
            params = dict(
                        key=API_KEY,
                        misnr=racenr,
                    )
        else:
            params = dict(
                        key=API_KEY,
                        race=1,
                        activeonly=1,
                    )
            
        response = requests.get(url=endpoint, params=params)
        data = response.json()
        with getMissionsFromSARL.safe_open_w(filename) as outfile:
            json.dump(data, outfile)

    return(data)


def sendMarks(racenr):
    data = getAPIData(racenr, 'marks')
    lastRequest = timestamp2Emoji(f'{DATAPATH}/marks_{racenr}.json') # datetime.fromtimestamp(creation_date(f'{DATAPATH}/marks_{racenr}.json')).strftime("%m/%d/%Y, %H:%M:%S")
    marks = [()]
    for mark in data['missioncourse']:
        marks.append((mark['miclat'], mark['miclon'], mark['mictypename']))
    filename = f'{DATAPATH}/course_{racenr}.gpx'
    with getMissionsFromSARL.safe_open_w(filename) as file:
        file.write('<?xml version="1.0"?>\n')
        file.write('<gpx>\n')
        for count in range(1, len(marks)):
            file.write(f"<wpt lat=\"{marks[count][0]}\" lon=\"{marks[count][1]}\">\n")
            file.write(f'   <name>VG 2020 mark {str(count)} ({marks[count][2]})</name>\n')
            file.write('</wpt>\n')
        file.write('</gpx>')
    return(lastRequest)


def prepareData(jsondata):
    data = []
    for result in jsondata['leaderboard'][0]['results']:
        data.append(result)

    for result in data:
        result['ubtspeed'] = round(result['ubtspeed'] * 1.94384, 1)
        result['ubtheading'] = round(result['ubtheading'])
        result['distancetonextmark'] = round(result['distancetonextmark'], 1)
        if result['ubtspeed'] < 0.1:
            result['ubtspeed'] = 0
        result['distancetonextmark'] = round(result['distancetonextmark'], 2)

    df = pd.DataFrame(data)
    df = df.drop(['racetime', 'racing', 'resultdescr', 'misnr', 'btptype',
                    'usrnr', 'teanr', 'ubtnr', 'finished', 'status'], axis=1)
    df = df.reindex(['rank','usrname','ubtname','ubtspeed', 'ubtheading', 'wind', 
                        'nextmarknr', 'distancetonextmark', 'pos', 'finishtime'], axis=1)
    df = df.rename(columns={'usrname':'Username',
                    'ubtheading':'Heading',
                    'distancetonextmark':'DTM',
                    'ubtspeed':'Speed',
                    'pos':'Position',
                    'rank':'Rank',
                    'ubtname':'Boat name',
                    'nextmarknr':'Next mark',
                    'wind':'Wind',
                    'finishtime':'Finish time',
    })
    dfsmall = df.drop(['Heading', 'Wind', 
                        'Next mark', 'Position', 'DTM', 'Boat name'], axis=1)
    dfsmall = df.reindex(['Rank','Username'], axis=1)

    df = df.applymap(str)
    dfsmall = dfsmall.applymap(str)
    df.columns = ['Rank', 'Sailor', 'Boat', 'Speed', 'Heading', 'Wind', 'Next mark', 
                'DTM', 'Position', 'Finish time']
    # df = df.head()
    return(df, dfsmall)


def dfToPdf(racenr, df):
    # from https://stackoverflow.com/questions/33155776/export-pandas-dataframe-into-a-pdf-file-using-python
    filename = f'{DATAPATH}/lb_{racenr}.pdf'
    fig, ax = plt.subplots(figsize=(12, 4))
    ax.axis('tight')
    ax.axis('off')
    the_table = ax.table(cellText=df.values, colLabels=df.columns, loc='center')
    if os.path.exists(filename):
        os.remove(filename)
    pp = PdfPages(filename)
    pp.savefig(fig, bbox_inches='tight')
    pp.close()
    lastRequest = timestamp2Emoji(f'{DATAPATH}/data_{racenr}.json') # datetime.fromtimestamp(creation_date(f'{DATAPATH}/data_{racenr}.json')).strftime("%m/%d/%Y, %H:%M:%S")
    return(lastRequest)


def lb2ascii(racenr, df, dfsmall):
    arr = df.to_numpy()
    header = ['Rank', 'Sailor', 'Boat', 'Speed', 'Heading', 'Wind', 'Next mark', 
              'DTM', 'Position', 'Finish time']
    table = tt.to_string(arr, header=header, alignment="rllrrlrrll")
    print(table)

    arr = dfsmall.to_numpy()
    header_small = ['Rank', 'Sailor']
    table_small = tt.to_string(arr, header=header_small, alignment="lr")

    with open(f'{DATAPATH}/lb_{racenr}.txt', 'w') as f:
        f.write(table)
    
    lastRequest = timestamp2Emoji(f'{DATAPATH}/data_{racenr}.json') # datetime.fromtimestamp(creation_date(f'{DATAPATH}/data_{racenr}.json')).strftime("%m/%d/%Y, %H:%M:%S")
    return(lastRequest, table_small)


def sendLb(racenr):
    lb = getAPIData(racenr, 'lb')
    df, dfsmall = prepareData(lb)
    lastRequest = dfToPdf(racenr, df)
    # lastRequest, tiny_lb = lb2ascii(racenr, df, dfsmall)
    return(lastRequest)


def fastesSailorsInRace(racenr: str):
    data = getAPIData(racenr)
    
    filename = f'{DATAPATH}/lb_{racenr}.json'
    lastRequest = timestamp2Emoji(f'{DATAPATH}/data_{racenr}.json') # datetime.fromtimestamp(creation_date(f'{filename}')).strftime("%m/%d/%Y, %H:%M:%S")

    for result in data['leaderboard'][0]['results']:
        result['ubtspeed'] = round(result['ubtspeed'] * 1.94384, 1)

    df = pd.DataFrame(data['leaderboard'][0]['results'])
    df = df.sort_values('ubtspeed', ascending=False)
    df = df.filter(['ubtname','ubtspeed', 'usrname'], axis=1)
    df.rename(columns={'ubtname':'Boat name',
                          'ubtspeed':'Speed (kt)',
                          'usrname':'Sailor'}, 
                 inplace=True)
    df = df.reindex(['Boat name','Sailor','Speed (kt)'],axis=1)
    text = df.to_string(index=False)
    return(lastRequest, text)


def sendBoat(boatname, racenr):
    lb = getAPIData(racenr, 'lb')
    lastRequest = timestamp2Emoji(f'{DATAPATH}/data_{racenr}.json') # datetime.fromtimestamp(creation_date(f'{DATAPATH}/data_{racenr}.json')).strftime("%m/%d/%Y, %H:%M:%S")
    text = f'Sorry, but a boat with the name *{boatname}* in race no. {racenr} was not found or the boat is not actively racing (DNF, finished).'
  
    for result in lb['leaderboard'][0]['results']:
        if result['ubtname'].lower() == boatname.lower():
            if result['racing'] == 1:
                rank = result['rank']
            if rank == '1':
                rank = f'🥇'
            if rank == '2':
                rank = f'🥈'
            if rank == '3':
                rank = f'🥉'

            text = f"**{result['usrname']}**s boat *{boatname}* status for race #{racenr}, generated {lastRequest}:\n" \
                  f"Race status: racing\n" \
                  f"Rank: {rank}\n" \
                  f"Speed: {str(round(result['ubtspeed'] * 1.94384, 2))} kn\n" \
                  f"Heading: {str(round(result['ubtheading'], 1))}°\n" \
                  f"Wind: {result['wind']}\n" \
                  f"Position: {result['pos']}\n" \
                  f"Next mark No: {result['nextmarknr']}\n" \
                  f"Distance to next mark: {round(result['distancetonextmark'], 2)} nm"
        if result['finished'] == 1:
            rt = seconds_to_days_hours_minutes(result['racetime'])
            text = f"**{result['usrname']}**s boat *{boatname}* status for race no. {racenr}, generated {lastRequest}:\n" \
                f"Race status: {result['status']} 🏁\n" \
                f"Rank: {result['rank']}\n" \
                f"Race time: {rt}\n"

    return(text)

def sendBoatBadge(boatname, racenr):
    lb = getAPIData(racenr, 'lb')
    dtlnumerical = distance_to_leader(boatname, racenr)
    if dtlnumerical!=0:
        distanceToLeader = str(round(dtlnumerical, 2))
    else:
        distanceToLeader = "not available"
    lastRequest = timestamp2Emoji(f'{DATAPATH}/data_{racenr}.json') # datetime.fromtimestamp(creation_date(f'{DATAPATH}/data_{racenr}.json')).strftime("%m/%d/%Y, %H:%M:%S")
    for result in lb['leaderboard'][0]['results']:
        boatname = boatname.lower()
        bt = result['ubtname'].lower()
        if bt == boatname:
            image = statusBadge(playername=result['usrname'],
                        boatname=result['ubtname'],
                        race=racenr,
                        timestamp=lastRequest,
                        status='Racing',
                        rank=result['rank'],
                        speed=f"{str(round(result['ubtspeed'] * 1.94384, 2))} kn",
                        heading=f"{str(round(result['ubtheading'], 1))}°",
                        wind=result['wind'],
                        position=result['pos'],
                        dtl=distanceToLeader,
                        dtm=f"{round(result['distancetonextmark'], 2)} nm",
                        nextmark=result['nextmarknr'],
                        )
            pass
    return(image)


def creation_date(path_to_file):
    """
    Try to get the date that a file was created, falling back to when it was
    last modified if that isn't possible.
    See http://stackoverflow.com/a/39501288/1709587 for explanation.
    """
    if platform.system() == 'Windows':
        return os.path.getctime(path_to_file)
    else:
        stat = os.stat(path_to_file)
        try:
            return stat.st_birthtime
        except AttributeError:
            # We're probably on Linux. No easy way to get creation dates here,
            # so we'll settle for when its content was last modified.
            return stat.st_mtime


def seconds_to_days_hours_minutes(seconds):
    day = seconds // (24 * 3600)
    seconds = seconds % (24 * 3600)
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60
    seconds = seconds
    return('%d days %d hours %d minutes %d seconds' % (day, hour, minutes, seconds))


def timestamp2Emoji(filename):
    ts = datetime.fromtimestamp(creation_date(f'{filename}'))
    lastRequest = f'{ts.strftime("%m/%d/%Y, %H:%M:%S")} UTC'
    """
    postfix = ''
    ampm = 'am'
    if ts.minute < 45 and ts.minute > 15:
        postfix = "30"
    if ts.hour > 12:
        hour = ts.hour - 12
        ampm = 'pm'

    oneto9 = [':zero:', ':one:', ':two:', ':three:', ':four:', ':five:', ':six:', ':seven:', ':eight:', ':nine:']

    if ts.month < 10:
        emojimonth = oneto9[ts.month]
    elif ts.month == 10:
        emojimonth = f'{oneto9[1]} {oneto9[0]}'
    elif ts.month == 11:
        emojimonth = f'{oneto9[1]} {oneto9[1]}'
    elif ts.month == 12:
        emojimonth = f'{oneto9[1]} {oneto9[2]}'

    strday = str(ts.day)
    if len(strday) < 10:
        emojiday = f'{oneto9[0]} {oneto9[ts.day]}'
    else:
        tenth = strday[0:1]
        ones = strday[1:1]
        emojiday = f'{oneto9[int(tenth)]} {oneto9[int(ones)]}'

    emojiclock = f':clock{hour}{postfix}:'
    return(f'{emojiday} / {emojimonth} - {emojiclock} {ampm} (UTC)')
    """
    return(lastRequest)


def LeaderPosition(racenr):
    lb = getAPIData(racenr, 'lb')
    lastRequest = timestamp2Emoji(f'{DATAPATH}/data_{racenr}.json') # datetime.fromtimestamp(creation_date(f'{DATAPATH}/data_{racenr}.json')).strftime("%m/%d/%Y, %H:%M:%S")
    for result in lb['leaderboard'][0]['results']:
        if result['rank'] == '1':           
            boat = result['ubtname']
            player = result['usrname']
            lat, lon = getBoatPosition(player, boat)
            return(lat, lon)

def boatPosition(boatname, racenr):
    boatname = boatname.lower()
    lb = getAPIData(racenr, 'lb')
    lastRequest = timestamp2Emoji(f'{DATAPATH}/data_{racenr}.json') # datetime.fromtimestamp(creation_date(f'{DATAPATH}/data_{racenr}.json')).strftime("%m/%d/%Y, %H:%M:%S")
    for result in lb['leaderboard'][0]['results']:
        btname = result['ubtname'].lower()
        if btname == boatname: #result['ubtname']
            boat = result['ubtname']
            player = result['usrname']
            lat, lon = getBoatPosition(player, boat)
            return(lat, lon)

def distance_to_leader(boatname, racenr):
    lat1, lon1 = LeaderPosition(racenr)
    if lat1!=0 and lon1 !=0:
        lat2, lon2 = boatPosition(boatname, racenr)
        dtl = haversine(lat1, lon1, lat2, lon2)
        return(dtl)
    else:
        return 0

def getBoatPosition(username, boatname):
    params = dict(
    key=API_KEY,
    usrname=username,
    )
    url = f'{API_BASE_URL}/TrackAllBoats.pl'
    resp = requests.get(url=url, params=params)
    data = resp.json()
    for boat in data['boats']:
        if boat['ubtname'] == boatname:
            lat = boat['ubtlat']
            lon = boat['ubtlon']
            return(lat, lon)
    return(0, 0)


def getBoatNameFromRank(rank, racenr):
    lb = getAPIData(racenr, 'lb')
    boatname = 'none'
    for result in lb['leaderboard'][0]['results']:
        if result['rank'] == rank:
            boatname = result['ubtname']
            pass
    return(boatname)


def getActiveRaces():
    missionsNo = []
    missionsName = []
    data = getAPIData("123","racelist")
    for r in data['missions']:
        missionsNo.append(r['misnr'])
        missionsName.append(r['mistitle'])
    return(missionsNo, missionsName)


def main():
    # result = round(dtl("dehumanizer", '34744'), 2)
    # print(str(result))
    result = getBoatNameFromRank('3', '34744')
    print(result)
    pass

if __name__ == "__main__":
    getActiveRaces()
    main()