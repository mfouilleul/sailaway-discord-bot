from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import sys
import tempfile


def statusBadge(playername, 
                boatname, 
                race, 
                timestamp,
                status,
                rank,
                speed,
                heading,
                wind,
                position,
                dtl,
                dtm,
                nextmark,
                ):
    in_file = 'status-badge.png'
    # out_file = 'status-badge_out.png' 
    out_file = tempfile.NamedTemporaryFile(suffix='.png').name


    column1x = 70
    column2x = 470
    column3x = 470 + 250
    column1y = 90
    column2y = 92
    linedistance_c1a = 80
    linedistance_c1b = 120
    linedistance_c2 = 100

    
    img = Image.open(in_file)
    draw = ImageDraw.Draw(img)
    fontbold = ImageFont.truetype('./fonts/Roboto-Bold.ttf', 40)
    fontregular = ImageFont.truetype('./fonts/Roboto-Regular.ttf', 40)
    fontitalic = ImageFont.truetype('./fonts/Roboto-Italic.ttf', 40)
    fontitalicsmall = ImageFont.truetype('./fonts/Roboto-Italic.ttf', 30)
    fontregularsmall = ImageFont.truetype('./fonts/Roboto-Regular.ttf', 30)
    text = 'Player'
    draw.text((column1x, column1y), text, (255, 255, 255), font=fontbold)
    column1y += linedistance_c1a
    draw.text((column1x, column1y), playername, (255, 255, 255), font=fontitalic)
    text = 'Boat'
    column1y += linedistance_c1b
    draw.text((column1x, column1y), text, (255, 255, 255), font=fontbold)
    column1y += linedistance_c1a
    draw.text((column1x, column1y), boatname, (255, 255, 255), font=fontitalic)
    text = 'Race'
    column1y += linedistance_c1b
    draw.text((column1x, column1y), text, (255, 255, 255), font=fontbold)
    column1y += linedistance_c1a
    draw.text((column1x, column1y), race, (255, 255, 255), font=fontitalic)
    text = 'Time'
    column1y += linedistance_c1b
    draw.text((column1x, column1y), text, (255, 255, 255), font=fontbold)
    column1y += linedistance_c1a
    date = timestamp.split(',')[0]
    time = timestamp.split(',')[1].lstrip()
    draw.text((column1x, column1y - 20), date, (255, 255, 255), font=fontitalicsmall)
    draw.text((column1x, column1y + 30), time, (255, 255, 255), font=fontitalicsmall)
    text = 'Status'
    draw.text((column2x, column2y), text, (255, 255, 255), font=fontregularsmall)
    draw.text((column3x, column2y), status, (255, 255, 255), font=fontitalicsmall)
    column2y += linedistance_c2
    text = 'Rank'
    draw.text((column2x, column2y), text, (255, 255, 255), font=fontregularsmall)
    draw.text((column3x, column2y), rank, (255, 255, 255), font=fontitalicsmall)
    column2y += linedistance_c2
    text = 'Speed'
    draw.text((column2x, column2y), text, (255, 255, 255), font=fontregularsmall)
    draw.text((column3x, column2y), speed, (255, 255, 255), font=fontitalicsmall)
    column2y += linedistance_c2
    text = 'Heading'
    draw.text((column2x, column2y), text, (255, 255, 255), font=fontregularsmall)
    draw.text((column3x, column2y), heading, (255, 255, 255), font=fontitalicsmall)
    column2y += linedistance_c2
    text = 'Wind'
    draw.text((column2x, column2y), text, (255, 255, 255), font=fontregularsmall)
    draw.text((column3x, column2y), wind, (255, 255, 255), font=fontitalicsmall)
    column2y += linedistance_c2
    text = 'Position'
    draw.text((column2x, column2y), text, (255, 255, 255), font=fontregularsmall)
    draw.text((column3x, column2y), position, (255, 255, 255), font=fontitalicsmall)
    column2y += linedistance_c2
    text = 'Dist. to Leader'
    draw.text((column2x, column2y), text, (255, 255, 255), font=fontregularsmall)
    draw.text((column3x, column2y), f'{dtl} nm', (255, 255, 255), font=fontitalicsmall)
    # draw.text((column3x, column2y), 'not available', (255, 255, 255), font=fontitalicsmall)
    column2y += linedistance_c2
    text = f'DTM (#{nextmark})'
    draw.text((column2x, column2y), text, (255, 255, 255), font=fontregularsmall)
    draw.text((column3x, column2y), dtm, (255, 255, 255), font=fontitalicsmall)
    img.save(out_file)

    return(out_file)