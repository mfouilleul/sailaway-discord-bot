# Add bot to server: 
# https://discordapp.com/api/oauth2/authorize?client_id=769912157860003840&permissions=247808&scope=bot
import os
from datetime import datetime, timedelta
import discord
import pandas as pd
import logging
import requests
import graypy
import git
import pathlib
from discord.ext import commands
from apscheduler.schedulers.background import BackgroundScheduler
from dotenv import load_dotenv
from sailaway import sendLb, sendBoat, sendMarks, creation_date, fastesSailorsInRace, sendBoatBadge, getBoatNameFromRank
import getMissionsFromSARL

load_dotenv(verbose=True)

DATAPATH = os.getenv('DATAPATH')
# LOG_SERVER_ADDRESS = os.getenv('GRAYLOG_SERVER')
# LOG_SERVER_PORT = int(os.getenv('GRAYLOG_PORT'))
TOKEN = os.getenv('DISCORD_TOKEN')
# LOG_SERVER_ADDRESS = os.getenv('GRAYLOG_SERVER')
# LOG_SERVER_PORT = int(os.getenv('GRAYLOG_PORT'))
MISSION = os.getenv('MISSION')
LASTREQUEST = datetime.now()

# Statistics
USERS = []
COMMANDS = []

description = 'Discord bot that offers some tools to help you keeping track of your Sailaway race.'

bot = commands.Bot(command_prefix='!', description=description, intents=discord.Intents.default())
sched = BackgroundScheduler()

# my_logger = logging.getLogger(__name__)
# my_logger.setLevel(logging.DEBUG)
# handler = graypy.GELFUDPHandler(LOG_SERVER_ADDRESS, LOG_SERVER_PORT)
# my_logger.addHandler(handler)


@bot.event
async def on_ready():
    print(f'Sailaway Discord Bot has started and is awaiting incoming events')
    print(f'{bot.user.name} has connected to Discord!')

""" 
@bot.event
async def on_member_join(member):
    # my_logger.info(f'New user {member.name} joined')
    await member.create_dm()
    await member.dm_channel.send(
        f'Hi {member.name}, welcome to the *Sailaway Vendée Globe* Discord server!'
    )
 """

@bot.command(name='leaderboard', 
                help='Sends the current leader board of a race',
                brief='Send leader board',
                aliases=['lb'])
async def leaderboard(ctx, racenr=MISSION):
    usageTracker(ctx, 'lb')
    print(f'{ctx.author} requested leader board for race {racenr}')
    if getMissionsFromSARL.raceExist(racenr):
        await ctx.send('🏆 Generating the leader board may take a minute, stay tuned! ⏳')
        lastrequest = sendLb(racenr)
        f = discord.File(f'{DATAPATH}/lb_{racenr}.pdf', filename=f'leaderboard_{racenr}.pdf')
        text = f"Leader board for race #{racenr} from {lastrequest}, due to the large number of contestants only available as pdf attachment:\n\n"
        # text_length = len(tiny_lb)
        # # my_logger.debug(f'Tiny leader board length: {text_length}')
        # if text_length < 1800:
            # text += f"Tiny leader board:\n```\n{tiny_lb}\n```"
        # else:
            # text += '⚠️ The leader is board too long for a Discord text message (max. 2000 characters). For full leader board, see attached file.'
        # await ctx.send(text, file=f)
        await ctx.send(text, file=f)
        ntfy(f'Leaderboard für Race {racenr} an *{ctx.author.name}* gesendet.')
    else:
        await invalidRaceMessage(ctx, racenr)

@bot.command(name='boatsbyspeed',
                help='Sends a list of boats in a race sorted by boat speed',
                aliases=['bs'])
async def boatsbyspeed(ctx, racenr=MISSION):
    usageTracker(ctx, 'boatsbyspeed')
    if getMissionsFromSARL.raceExist(racenr):
        print(f'{ctx.author} request boatsbyspeed for race {racenr}')
        lastrequest, speedlist = fastesSailorsInRace(racenr)
        text = f'💨 Current boat speeds in race #{racenr} at {lastrequest}:\n\n'
        text += f'```\n{speedlist}\n```'
        if len(text) > 1999:
            speedlist = f'Current boat speeds in race #{racenr} at {lastrequest}:\n\n' + speedlist
            filename = f'{DATAPATH}/boatspeed.txt'
            with open(filename, 'w') as bs:
                bs.write(speedlist)
            f = discord.File(filename, filename=f'boatspeed_{racenr}.txt')
            await ctx.send(f'💨 Current boat speeds in race #{racenr} at {lastrequest} as text file.', file=f)
            ntfy(f'Boat speeds für Race {racenr} als Textanhang an *{ctx.author.name}* gesendet.')
        else:
            await ctx.send(text)
            ntfy(f'Boat speeds für Race {racenr} an *{ctx.author.name}* gesendet.')
    else:
        await invalidRaceMessage(ctx, racenr)

@bot.command(name='marks',
                help='Sends the current course of a race as *GPX* file',
                brief='Send race marks')
async def marks(ctx, racenr=MISSION):
    usageTracker(ctx, 'marks')
    print(f'{ctx.author} requested race marks for race {racenr}')
    await ctx.send('📍 Generating the GPX file will take a few seconds... ⏳')
    lastrequest = sendMarks(racenr)
    f = discord.File(f'{DATAPATH}/course_{racenr}.gpx', filename=f'course_{racenr}.gpx')
    text = f"Here are the race marks for race #{racenr} from {lastrequest}"
    await ctx.send(text, file=f)
    ntfy(f'Marks für Race {racenr} an *{ctx.author.name}* gesendet.')

@bot.command(name='myraces', 
                help='Sends a list of all races a player competes in',
                brief='Your races',
                aliases=['mr', 'races'])
async def myraces(ctx, username: str):
    usageTracker(ctx, 'myraces')
    if not username:
        text = 'You have to give me your Sailaway username. Example: `/myraces "elpatron"`.'
        # my_logger.debug(f'{ctx.author}: myraces without player name')
    else:
        print(f'{ctx.author} requested his race list')
        text = getMissionsFromSARL.findRaces(username)
    await ctx.send(text)
    ntfy(f'Spieler-Races für {username} an *{ctx.author.name}* gesendet.')

@bot.command(name='status', 
                help='Sends detailed information about a users boat',
                brief='Send boat status',
                aliases=['st', 'boat'])
async def status(ctx, boatname = '', racenr=MISSION):
    usageTracker(ctx, 'status')
    if boatname == '':
        # my_logger.debug(f'{ctx.author}: status without boat name')
        text = 'You have to give me your boat name. Example: `/status "Dehumanizer"`.'
        await ctx.send(text)
    else:
        if getMissionsFromSARL.raceExist(racenr):
            if boatname.isnumeric():
                print(f'Resolving rank {boatname} to boat name')
                boatname = getBoatNameFromRank(boatname, racenr)
            print(f'{ctx.author} requested status for boat {boatname} in race {racenr}')
            # status = sendBoat(boatname, racenr)
            # await ctx.send(status)
            image = sendBoatBadge(boatname, racenr)
            f = discord.File(image, filename=f'boatstatus.png')
            text = f'Boat status for {boatname}'
            await ctx.send(text, file=f)
            ntfy(f'Status für boot {boatname} in Race {racenr} an *{ctx.author.name}* gesendet.')
            os.remove(image)
        else:
            await invalidRaceMessage(ctx, racenr)

@bot.command(name='about', 
                help='Shows some information about the bot',
                brief='About the bot',
                aliases=['ahoi'])
async def about(ctx):
    usageTracker(ctx, 'about')
    print(f'{ctx.author}: Sending about message')
    repo = git.Repo(search_parent_directories=True)
    sha = repo.head.object.hexsha
    text = f'Ahoi Sailor {ctx.author}! ⛵\n\n' \
           'I am a Discord bot that offers some tools to help you to keep track of your Sailaway race.\n\n' \
           'They might be helpful if you compete in a long race in times where you don´t have access to the game.\n' \
           'If you have any questions or wishes, don´t hesitate to contact my father @elpatron#4253.\n\n' \
           'I offer you some commands which you can send to me in any supported channel or as private message.\n\n' \
           'The requested data is as current as possible, although it will be cached for 30 minutes between ' \
           'requests to prevent overloading the game server API.\n\n' \
           'To see a list of supported commands, send `!help`\n\n' \
           'For detailed information and examples, read the online documentation at ' \
           'https://gitlab.com/m-busche/sailaway-discord-bot/-/blob/master/readme.md\n' \
           'My source code is available at https://gitlab.com/m-busche/sailaway-discord-bot\n\n' \
           f'*Debug information*: Sailaway Discord Bot git hash: `{sha}`'
    await ctx.send(text)

@bot.command(name='sarl', 
                help='Send a link to the Sailaway race log website SARL',
                brief='Send SARL Link',
                aliases=['rl'])
async def sarl(ctx, racenr=MISSION):
    usageTracker(ctx, 'sarl')
    print(f'{ctx.author}: Sending SARL link for race {racenr}')
    if getMissionsFromSARL.raceExist(racenr):
        await ctx.send(f'🏃🏽‍♀️ Direct link to the race tracker: https://sarl.ingenium.net.au/sarl?racenr={racenr}')
    else:
        await invalidRaceMessage(ctx, racenr)

@bot.command(name='readme', 
                help='Send a link to bot´s documentation and usage examples',
                brief='Send README Link',
                aliases=['doc'])
async def readme(ctx):
    usageTracker(ctx, 'readme')
    print(f'{ctx.author}: Sending REAMDE link')
    await ctx.send('📖 Link to the bot´s README: https://gitlab.com/m-busche/sailaway-discord-bot/-/blob/master/readme.md')

@bot.command(name='polar', 
                help='Send Polar file for a boat',
                brief='Send Polar file')
async def polar(ctx, boatnumber='0'):
    ntfy(f'Polar file requestd.')
    await ctx.send('⚠️ This function is temporarily disabled.')

# @bot.command(name='polar', 
#                 help='Send Polar file for a boat',
#                 brief='Send Polar file')
# async def polar(ctx, boatnumber='0'):
#     usageTracker(ctx, 'polar')
#     if boatnumber not in ['2', '4', '5', '8', '9', '10']:
#         text = '⚠️ You have to send a valid number of the boat you want to have a polar file for (eg. `!polar 5`).\n\n' \
#                 'Supported boats:\n' \
#                 '2  - Mini Transat\n' \
#                 '4  - 52 ft Cruising Cat\n' \
#                 '5  - 50 ft Performance Cruiser\n' \
#                 '8  - 32 ft Offshore Racer\n' \
#                 '9  - 45 ft Ketch\n' \
#                 '10 - Imoca\n\n' \
#                 'Polar files for other boats are not usable due to a lack of data.\n' \
#                 'Polar files are from https://spp.elpatron.me/'
#         await ctx.send(text)
#     else:
#         print(f'{ctx.author}: Sending polar file for boat {boatnumber}')
#         filename = f"{os.getenv('POLARPATH')}/polar_{boatnumber}_mean_f100.csv"
#         timestamp = datetime.fromtimestamp(creation_date(f'{filename}')).strftime("%m/%d/%Y, %H:%M:%S")
#         text = f'⛵ Here is the polar file for boat {boatnumber}, built at {timestamp} UTC.'
#         f = discord.File(filename, filename=f'polar_{boatnumber}_mean.csv')
#         await ctx.send(text, file=f)


@bot.command(name='links', 
                help='Send useful Sailaway links',
                brief='Send useful links',
                aliases=['li'])
async def links(ctx):
    usageTracker(ctx, 'links')
    print(f'{ctx.author}: Sending useful links')
    text = '🌍 Useful *Sailaway* links\n\n' \
            '🏠 Sailaway homepage: https://sailaway.world/\n' \
            '🐧 Sailaway account page: https://sailaway.world/myaccount.pl\n' \
            '🏄 SARL Race Log: https://sarl.ingenium.net.au/\n' \
            '⚙️ Polar Portal: https://spp.elpatron.me/polarfiles\n' \
            '🌊 Pirosail boat tracker: http://pirosail.ovh/\n' \
            '📘 Sailaway Wiki (incomplete): https://sailawaywiki.elpatron.me/en/home'
    await ctx.send(text)
    ntfy(f'Links gesendet')

@bot.command(name='whichrace', 
                help='Search race by title or number',
                brief='Search race',
                aliases=['wr'])
async def whichrace(ctx, searchtext = ''):
    usageTracker(ctx, 'whichrace')
    print(f'{ctx.author}: Sending myraces with search {searchtext}')
    if searchtext == '':
        lastrequest, missions = getMissionsFromSARL.sendAllRaces()
        text = f'List of all active races from {lastrequest}:\n\n'
        text += f'```\n{missions}\n```'
        if len(text) > 1950:
            msg = '😒 The race list exeeds Discord´s length limit, ' \
                    f'it will be sent as file attachment. Data based on request at {lastrequest}'
            filename = f'{DATAPATH}/missions.txt'
            with open(filename, 'w') as tmpfile:
                tmpfile.write(missions)
            f = discord.File(filename, filename=f'activeraces.txt')
            # my_logger.debug('Sent complete race list as file')
            await ctx.send(msg, file=f)
            ntfy(f'Raceliste als Anhang an *{ctx.author.name}* gesendet')
        else:
            # my_logger.debug('Sent complete race list as text')
            await ctx.send(text)
            ntfy(f'Raceliste an *{ctx.author.name}* gesendet')

    else:
        if searchtext.isnumeric():
            if getMissionsFromSARL.raceExist(searchtext):
                text = getMissionsFromSARL.raceNumberToTitle(searchtext)
            else:
                await invalidRaceMessage(ctx, searchtext)
        else:
            text = getMissionsFromSARL.raceTitleToNumber(searchtext)
        await ctx.send(text)
        ntfy(f'Raceliste an *{ctx.author.name}* gesendet')

@bot.command(name='stats', hidden=True)
async def stats(ctx, subcommand=''):
    if subcommand == '':
        text = f'Number of users: {len(USERS)}\n' \
                f'Received commands: {len(COMMANDS)}'
    elif subcommand == 'u':
        text = 'Today´s users:\n\n```\n'
        for user in USERS:
            text += f'{user}\n'
        text += '```'
    elif subcommand == 'c':
        text = 'Today´s commands:\n\n```\n'
        for cmd in COMMANDS:
            text += f'{cmd}\n'
        text += '```'
    await ctx.author.send(text)
    ntfy(f'Statistik an *{ctx.author.name}* gesendet')

# https://discordpy.readthedocs.io/en/latest/ext/commands/api.html?highlight=commands%20errors#exceptions
@bot.event
async def on_message_error(ctx, error):
    if isinstance(error, discord.ext.commands.errors.CommandNotFound):
        # my_logger.warn('Unknown command')
        await ctx.send('Unknown command')

def pingHeartbeat():
    print(f'Sending heartbeat ping')
    requests.get("https://hc-ping.com/801026a1-5a79-409f-9f9e-22b5827d3564", timeout=10)

def updRaces():
    filename = f'{DATAPATH}/races.json'
    p = pathlib.Path(filename)
    if p.is_file():
        lastRequest = datetime.fromtimestamp(creation_date(filename))
        timeDelta = datetime.now() - lastRequest
        if timeDelta.seconds > 60 * 60 * 3:
            print(f'Scheduled race update, file is too old')
            print('Scheduled race update, file is too old')
            ntfy(f'Raceliste upgedatet')
            getMissionsFromSARL.updateRaces()
    else:
        print(f'Scheduled race update, file doesn´t exist')
        print('Scheduled race update, file doesn´t exist')
        getMissionsFromSARL.updateRaces()
        ntfy(f'Raceliste upgedatet')


async def invalidRaceMessage(ctx, racenr):
    print(f'{ctx.author}: Received invalid race number {racenr}')
    await ctx.send(f'🙇🏼 Sorry, but a (active) race with the number *{racenr}* does not exist. ' \
                        'Try to search the race number using `!help whichrace`.')


def usageTracker(ctx, command: str):
    if not ctx.author.name in USERS:
        USERS.append(ctx.author.name) #USERS[0].name
    COMMANDS.append(command)

def resetStats():
    print(f'Resetting usage statistics')
    USERS = []
    COMMANDS = []


def ntfy(message):
    requests.post("https://ntfy.sh/UOUv2W4wMfmluFfW",
        data=f"{message}".encode(encoding='utf-8'))


if __name__ == "__main__":
    updRaces()
    sched.add_job(pingHeartbeat, 'interval', minutes=15)
    sched.add_job(updRaces, 'interval', minutes=60*3)
    # sched.add_job(resetStats, trigger='cron', month='*', day='*', hour='0', minute='0')
    # sched.add_job(resetStats, minutes=60*24)
    sched.start()

    bot.run(TOKEN)
