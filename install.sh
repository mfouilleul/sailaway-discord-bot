#!/bin/bash
sudo apt install make automake gcc g++ subversion python3-dev build-essential
git clone https://gitlab.com/m-busche/sailaway-discord-bot.git
cd sailaway-discord-bot
python3 -m venv .venv
source .venv/bin/activate
pip3 install -r requirements.txt
