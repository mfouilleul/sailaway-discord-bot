# SailawayRaceBot

[[_TOC_]]

## What Is A (Discord-) Bot?

*SailawayRaceBot* is a Discord bot. A Discord bot is an application that can answer commands sent by the user. For the user, a bot looks like any other user.

Discord bots react to commands sent by the user - either in a chat channel or as a private message to the bot.

## SailawayRaceBot Features

*SailawayRaceBot* offers some commands that help you keep track of your boat competing in a [*Sailaway*](https://sailaway.world/) race. Although, there are some tools and websites that offer similar features, 
*SailawayRaceBot* can offer you vital race information whe you don´t have a PC to hand, as it is fast, 
offers current data and runs on your mobile (Discord app).

## How To Send A Command To The Bot

Each command has to be sent as a message, either in a Discord chat channel (where the bot has access rights) or as private message to the bot user `SailawayRaceBot#3846`. The bot sends his answer in the context, the command was sent in. E.g.: If a command was sent in a public channel, the answer is written to that channel. Private messages are answered private.

Commands to *SailawayRaceBot* have to start with `!`. 

**Note**:

You have to enclose boat- or user names in `"` if they contain spaces!

## Supported Commands

*SailawayRaceBot* supports the following commands:

| Command | Syntax | Example link |
| --- | --- | --- |
| `!help` | Sends a help text |   |
| `!about` | Sends a text with information about the bot |   | 
| `!lb <race number>` | Sends the current leaderboard of a race | [Example](#send-a-leader-board)  |
| `!marks <race number>` | Sends race marks of a race as GPX file for import into *qtVlm* |   |
| `!myraces <user name>` | Sends all races a user competes in | [Example](#send-all-races-a-user-competes-in)  |
| `!status <boat name> <race number>` | Sends detailed information about a users boat | [Example](#send-a-boats-status) |
| `!sarl <race number>` | Sends a link to the Sailaway race log website *SARL* |   |
| `!whichrace <search text>` | Search race by title or number |  [Example](#search-a-race-number) |
| `!polar <boat number>` | Send polar the file for a boat |   |
| `!boatsbyspeed <race number>` | Sends a list of boats in a race sorted by boat speed |   |
| `!readme` | Send a link to this README |   |
| `!links` | Send a list of important *Sailaway* web links |   |

The `<race number>` is the unique identifier of a race. You can get a race number by using `!whichrace` - see examples.

The list of supported `<boat number>` and boat names for the `!polar` command is sent when `!polar` is sent without or invalit boat number.

**Note:**

Note that only current races are supported. Current races are listed as *Current* on [SARL](https://sarl.ingenium.net.au/).

### Examples

Here are some examples for the most important commands, more will be added later:

#### Search A race number

Most commands require a race number to identify the race you want to see. You can get the race number of the race of interest by sending the command `!whichrace <search text>`. If `<search text>` is omitted, a list of all active races will be sent.

Example 1:

`!whichrace bornholm` 

Answer:

`Race bornholm matched for race #34783.
SARL: https://sarl.ingenium.net.au/sarl?racenr=34783`

Example 2:

if you send `!whichrace` without search text, a list of all races will be sent. The list maybe longer than Discord´s limit for text messages. In that case it will be sent as text file.

`!whichrace`

Answer:

```
Race number                                                     Race title
      34781                           Régate  SCF Saint Martin  - 7.036 NM
      34839  Les Sables-d'Olonne <-----> Plateau De Rochebonne - 73.738 NM
      34802                                Régate  SCF Martique - 7.040 NM
      34759                                        Penguin Race - 6.244 NM
[...]
      34783                                      Bornholm Race - 38.844 NM
      34565                   The Pacific Ocean Race (Leg 2) - 3268.463 NM
      34747                              Escobar Run - 52 Cat - 912.793 NM
      34776                   Tampa Triangle - 01 - 50 class - 1209.777 NM
```

**Note:**

The *SailawayRaceBot* usually defaults to a popular race. At the moment, the *Vendée Globe 2020/21* (#34744) is the default race. If you omit `race number`, the results are tzaken from the Vendée Globe race.

#### Send A Leader Board

Just send `!lb <race number>` to get a current leader board for a race.

Example:

`!lb 34709`

Answer:

```
Leader board for race #34709 from 10/31/2020, 00:06:04 UTC, detailed version in text attachment:

Tiny leader board:
┌──────┬──────────────────────┐
│ Rank │               Sailor │
╞══════╪══════════════════════╡
│ 1    │              Dbird64 │
├──────┼──────────────────────┤
│ 2    │ isla hormiga c.palos │
├──────┼──────────────────────┤
│ 3    │           Bionic2 NL │
├──────┼──────────────────────┤
│ 4    │                  Joe │
├──────┼──────────────────────┤
│ 5    │        stpetegilbert │
├──────┼──────────────────────┤
│ 6    │             MertEmre │
├──────┼──────────────────────┤
│ 7    │           GranTubino │
├──────┼──────────────────────┤
│ 8    │                Jdubz │
├──────┼──────────────────────┤
│ 9    │            Freespeed │
├──────┼──────────────────────┤
│ 10   │              MatSail │
├──────┼──────────────────────┤
│ 11   │             VIPER777 │
├──────┼──────────────────────┤
│ 12   │                Tyler │
├──────┼──────────────────────┤
│ DNF  │             zoomarch │
└──────┴──────────────────────┘
```

**Note:**

A more detailed leader board is attached as text file.

#### Send A Boat´s Status

Send `!status <boat name> <race number>` to get detailed information about a racing boat.

Example:

`!status "Zuma" 34709`

Answer:

```
Dbird64s boat Zuma status for race no. 34709, generated 10/31/2020, 01:13:09 UTC:
Race status: racing
Rank: :first_place:
Speed: 17.91 kn
Heading: 79.2°
Wind: 211°, 23.6kn.
Position: S57°48.573' W38°43.293'
Next mark No: 4
Distance to next mark: 2678.41 nm
```

#### Send All Races A User Competes In

That´s easy: Send `!myraces <user name>` to see al races a user competes in.

Example:

`!myraces Blackboro`

Answer:

```
Result of race-search for player Blackboro:
Race: 34802 - Title: Régate  SCF Martique - Rank: 2 - Status: Finished
Race: 34781 - Title: Régate  SCF Saint Martin  - Rank: 1 - Status: Finished
```

## How To Add The Bot To You Own Discord Server

Adding *SailawayRaceBot* to your own server is easy:

Open [this link](https://discordapp.com/api/oauth2/authorize?client_id=769912157860003840&permissions=247808&scope=bot), log in to Discord and select the server you want the bot to be added. You have to be an administrator of the server you want the bot to be added to.
