from math import radians, sin, cos, asin, sqrt

# from https://blog.petehouston.com/calculate-distance-of-two-locations-on-earth/
def haversine(lon1, lat1, lon2, lat2):
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    result = 2 * 3958.756 * asin(sqrt(a))
    return(result)

